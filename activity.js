let http = require('http')

let course = [
	{
		courseID: "CCS101",
		name: "Programming 101",
		isActive: true
	},
	{
		courseID: "IT101",
		name: "Data Structures",
		isActive: true
	},
	{
		courseID: "HCI",
		name: "Human Computer Interaction",
		isActive: true
	}

]

http.createServer(function(req,res){
	if(req.url === "/courses" && req.method == "GET"){
		res.writeHead(200,{'Content-Type':'application/json'})
		res.end(JSON.stringify(course))
	}
	else if(req.url === "/courses" && req.method == "POST"){
		let requestBody = ""

		req.on('data',function(data){
			requestBody += data
		})

		req.on('end', function(){
			console.log(requestBody);
			requestBody = JSON.parse(requestBody)

			let newCourse = {
				courseID: requestBody.courseID,
				name: requestBody.name,
				isActive: requestBody.isActive
			}

			course.push(newCourse)
			console.log(course)

			res.writeHead(200,{"Content-Type": "application/json"})
			res.end(`Course ${newCourse.name} added`)
		})
	}
	else if(req.url === "/courses/getSingleCourse" && req.method == "POST"){
		let requestBody = ""

		req.on('data',function(data){
			requestBody += data
		})
		req.on('end', function(){
			console.log(requestBody)
			requestBody = JSON.parse(requestBody)

			let foundCourse = course.find((course)=>{
				console.log(course)
				return course.name === requestBody.name
			})

			console.log(foundCourse)

			if(foundCourse !== undefined){
				res.writeHead(200,{"Content-Type": "application/json"})
				res.end(JSON.stringify(foundCourse))
			} 
			else {
				res.writeHead(401,{"Content-Type": "application/json"})
				res.end("Course Not Found")
			}
		})
	}
	else if(req.url === "/courses/deleteSingleCourse" && req.method == "DELETE"){
		res.writeHead(401,{"Content-Type": "application/json"})
		res.end("Course deleted.")
		course.pop()
	}

}).listen(8000)